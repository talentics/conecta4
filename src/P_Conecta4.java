import java.util.Scanner;

public class P_Conecta4 {
    public static void main(String [] args) {
        int mLongitud = 7;
        int mAltura = 5;

        boolean jugar = false;
        boolean ganador = false;
        int jFila, jColumna;

        Scanner teclado = new Scanner(System.in);

        String [][] matTablero = new String[mAltura][mLongitud];

        System.out.println("\nPresiona 1 para jugar...");
        if (Integer.parseInt(teclado.nextLine()) == 1)
            jugar= true;

        while (jugar) {
                // NUEVO JUEGO
                inicializaTablero(matTablero, mLongitud, mAltura);
                int contador = 0;
                ganador = false;

                while (!ganador && contador < mAltura*mLongitud) {
                    // TURNO JUGADOR 1
                    if (!ganador) {
                        showTablero(matTablero, mLongitud, mAltura);
                        System.out.println("\nJUGADOR 1 (REDONDAS), introduce columna para tirada...");
                        jColumna = Integer.parseInt(teclado.nextLine());
                        jFila = cambiaValor(matTablero, mAltura, jColumna, 'O');
                        //System.out.println(matTablero[jFila][jColumna].equals("[O]"));
                        contador++;
                        ganador = evalSoluciones(matTablero, jFila, jColumna, 'O', mAltura, mLongitud);
                        if (ganador) {
                            System.out.println("\nGANADOR: JUGADOR 1 (REDONDAS)");
                            showTablero(matTablero, mLongitud, mAltura);
                        }
                    }

                    // TURNO JUGADOR 2
                    if (!ganador) {
                        showTablero(matTablero, mLongitud, mAltura);
                        System.out.println("\nJUGADOR 2 (CRUCES), introduce columna para tirada...");
                        jColumna = Integer.parseInt(teclado.nextLine());
                        jFila = cambiaValor(matTablero, mAltura, jColumna, 'X');
                        //System.out.println(matTablero[jFila][jColumna].equals("[X]"));
                        contador++;
                        ganador = evalSoluciones(matTablero, jFila, jColumna, 'X', mAltura, mLongitud);
                        if (ganador) {
                            System.out.println("\nGANADOR: JUGADOR 2 (CRUCES)");
                            showTablero(matTablero, mLongitud, mAltura);
                        }
                    }
                }

                // FIN DEL JUEGO
                System.out.println("\nPresiona 1 para jugar...");
                if (Integer.parseInt(teclado.nextLine()) == 1) {
                    jugar = true;
                }
                else {
                    System.out.println("Bye..!");
                    jugar=false;
                }

        }
    }

    public static void showTablero (String [][] mTablero, int nColumnas, int nFilas) {
        System.out.print(" Col: ");
        for(int j=0; j < nColumnas; j++) {
            System.out.print("["+j+"]");
        }
        System.out.print("\n");

        for (int i=0; i < nFilas; i++) {
            System.out.print("[F"+ i + "]: ");
            for(int j=0; j < nColumnas; j++) {
                System.out.print(mTablero[i][j]);
            }
            System.out.print("\n");
        }
    }

    public static void inicializaTablero (String [][] mTablero, int nColumnas, int nFilas) {
        for (int i=0; i< nFilas; i++) {
            for(int j=0; j< nColumnas; j++) {
                mTablero[i][j] = "[ ]";
            }
        }
    }

    public static int cambiaValor (String [][] mTablero, int nFilas, int j, Character nValor) {
        for (int i = nFilas-1; i >= 0; i--) {
            if (mTablero[i][j] == "[ ]") {
                mTablero[i][j] = "[" + nValor + "]";
                return i;
            }
        }
        return -1;
    }

    public static boolean evalSoluciones(String [][] mTablero, int i, int j, Character nValor, int nFilas, int nCol){
        // Filas
        if (evalFilas(mTablero, i, nCol, nValor)) {
            return (true);
        }
        // Columnas
        if (evalColumnas(mTablero, nFilas, j, nValor)) {
            return (true);
        }
        // Diagonal 1
        if(evalDiagonal1(mTablero, nFilas, nCol, nValor)) {
            return (true);
        }
        // Diagonal 2
        if(evalDiagonal2(mTablero, nFilas, nCol, nValor)) {
            return (true);
        }
        // Ningun Resultado
        return (false);
    }


    public static boolean evalFilas (String [][] mTab, int fila, int columnas, Character nValor)
    {
        int i, i2, tmp;
        boolean result = false;
        for (i=0; i< columnas-4; i++)
        {
            tmp=0;
            for (i2=i; i2<i+4; i2++)
                if (mTab[fila][i2].equals("[" + nValor + "]"))
                    tmp++;
            if (tmp==4)
                result = true;
        }
        return (result);
    }

    public static boolean evalColumnas (String [][] mTab, int filas, int columna, Character nValor)
    {
        int i, i2, tmp;
        boolean result = false;
        for (i=filas-1; i>3; i--)
        {
            tmp=0;
            for (i2=i; i2>i-4; i2--)
                if (mTab[i2][columna].equals("[" + nValor + "]"))
                    tmp++;
            if (tmp==4)
                result = true;
        }
        return (result);
    }

    public static boolean evalDiagonal1 (String [][] mTab, int filas, int columnas, Character nValor) {
        int i, j, n, tmp;
        boolean result = false;
        for(i=filas-1; i>2; i--)
        {
            for(j=3; j<columnas; j++)
            {
                tmp=0;
                for(n=0; n<4; n++)
                    if(mTab[i-n][j-n].equals("[" + nValor + "]"))
                        tmp++;
                if (tmp==4)
                    result = true;
            }
        }
        return (result);
    }

    public static boolean evalDiagonal2 (String [][] mTab, int filas, int columnas, Character nValor) {
        int i, j, n, tmp;
        boolean result = false;
        for(i=filas-1; i>2; i--)
        {
            for(j=0; j<columnas-3; j++)
            {
                tmp=0;
                for(n=0; n<4; n++)
                    if(mTab[i-n][j+n].equals("[" + nValor + "]"))
                        tmp++;
                if (tmp==4)
                    result = true;
            }
        }
        return (result);
    }
}

